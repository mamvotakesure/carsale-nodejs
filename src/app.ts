import express from 'express';
require('dotenv').config()
import logger from './utils/logger';
import connect from './utils/connect';
const app = express();



app.listen(4000, async () => {
    logger.info('Server listening on port 4000');
    await connect();
})