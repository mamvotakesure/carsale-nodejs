import mongoose from 'mongoose';
import config from "config"
import logger from './logger';
import process from 'process';


export async function connect() {
    const dbUri = config.get<string>('DBUri');
    try {
        await mongoose.connect(dbUri)
        logger.info('DB Connected successfully')
    } catch (err: any) {
        logger.error('Error during handling database connection')
        process.exit(1)
    }

}

export default connect